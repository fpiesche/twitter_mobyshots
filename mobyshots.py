from __future__ import print_function

import ConfigParser
import dateparser
import datetime
import json
from random import choice
import requests
import shutil
import tempfile
import time

from pyshorteners import Shortener
from mastodon import Mastodon
import twitter

import os


class MobyShots(object):

    def __init__(self):
        """Init."""

        # Mobygames-defined variables
        self.api_base_url = 'https://api.mobygames.com/v1/'
        self.hourly_limit = 360
        self.random_cache_time = 3600
        self.throttle_seconds = 1

        self.config_file = os.path.join(os.path.expanduser("~"), '.mobyshots.conf')
        self.config = ConfigParser.SafeConfigParser()
        self.config.read(self.config_file)
        self.first_request = time.time()
        self.last_request = 0
        self.total_requests = 0

    def log(self, message):
        timestamp = datetime.datetime.utcnow().strftime('%Y%m%d-%H%M%S')
        print('%s\t%s' % (timestamp, message))

    def api_request(self, api_call):
        """Perform a Mobygames API request."""

        # throttle to maximum allowed number of requests per second
        if time.time() < self.last_request + 1:
            time.sleep(self.throttle_seconds)

        # check if we've exceeded the hourly rate limit and sleep until the full hour if so
        if self.total_requests == self.hourly_limit and time.time() < self.first_request + 3600:
            sleep_time = int(self.first_request + 3601) - time.time()
            self.log('Reached hourly API rate limit, sleeping for %s seconds...' % sleep_time)
            time.sleep(sleep_time)
            self.total_requests = 0

        # build URL parameters for the call
        parameters = {'api_key': self.config.get('mobygames_api', 'api_key')}
        if 'screenshots' not in api_call:
            parameters.update({'format': 'normal', 'limit': '20'})

        # make the API call, set internal API rate limit tracking and return the response
        request = requests.get('%s%s' % (self.api_base_url, api_call), params=parameters)
        self.log('Requested %s' % request.url)
        request.raise_for_status()
        self.last_request = time.time()
        self.total_requests += 1
        return json.loads(request.text)

    def get_platform_name(self):
        self.log('Getting platform %s...' % self.platform)
        for platform in self.game['platforms']:
            if self.platform == platform['platform_id']:
                return platform['platform_name']

    def get_release_year(self):
        self.log('Getting release year for %s...' % self.game['title'])
        for platform in self.game['platforms']:
            if self.platform == platform['platform_id']:
                return dateparser.parse(platform['first_release_date']).year

    def get_games(self, game_id='random'):
        self.log('Getting games...')
        game_request = self.api_request('games/%s' % game_id)
        if game_id != 'random':
            return [game_request]
        else:
            return game_request['games']

    def get_screenshots(self, game):
        self.log('Getting screenshots for %s (%s)...' % (game['title'], game['game_id']))
        self.log('Game is available on platforms %s.' % ([platform['platform_id'] for platform in game['platforms']]))
        screenshots = {}
        for platform in game['platforms']:
            plat_id = platform['platform_id']
            shots = self.api_request('games/%s/platforms/%s/screenshots' %
                                     (game['game_id'], plat_id))['screenshots']
            if len(shots) > 0:
                screenshots[plat_id] = shots
        return screenshots

    def get_random_game_with_screenshots(self):
        games_shots = []
        while len(games_shots) == 0:
            games = self.get_games()
            games_shots = [game for game in games if game['sample_screenshots'] != {}]
            if len(games_shots) == 0:
                self.log('No games have screenshots, waiting for %s seconds...' % self.random_cache_time)
                time.sleep(self.random_cache_time)
        return choice(games_shots)

    def get_messages(self):
        # get platform name
        platform_name = self.get_platform_name()
        release_year = self.get_release_year()

        messages = {}

        services = {'twitter': 140, 'mastodon': 500}

        for service, max_length in services.items():

            if service == 'twitter':
                url = self.game['moby_url']
                url_length = 24
            elif service == 'mastodon':
                url = Shortener('Tinyurl').short(self.game['moby_url'])
                url_length = len(url) + 1

            messages[service] = '%s\n%s' % (self.shot['caption'], self.game['title'])

            if len(messages[service]) > max_length:
                self.log('Title+URL too long for %s, trimming title.' % service)
                messages[service] = '%s' % self.game['title'][:max_length - url_length]
            elif len(messages[service]) + len(platform_name) + 9 < max_length:
                self.log('Can fit platform and year, adding both.')
                messages[service] = '%s (%s, %s)' % (messages[service], platform_name, release_year)
            elif len(messages[service]) + 7 < 94:
                self.log('Can fit release year, adding.')
                messages[service] = '%s (%s)' % (messages[service], release_year)

            messages[service] = '%s\n%s' % (messages[service], url)

        return messages

    def tweet(self, message, image):
        twitter_api = twitter.Api(consumer_key=self.config.get('twitter_api', 'consumer_key'),
                                  consumer_secret=self.config.get('twitter_api', 'consumer_secret'),
                                  access_token_key=self.config.get('twitter_api', 'access_token_key'),
                                  access_token_secret=self.config.get('twitter_api', 'access_token_secret'))
        try:
            twitter_api.PostMedia(message, image.name)
        except twitter.error.TwitterError as error:
            self.log('Failed to tweet %s: %s' % (self.shot['image'], error.message))
            return False
        self.log('Successfully tweeted screenshot for %s' % self.game['title'])
        return True

    def toot(self, message, image):
        masto = Mastodon(client_id=os.path.join(os.path.expanduser("~"), '.mobyshots_mastodon_credentials'),
                         access_token=os.path.join(os.path.expanduser("~"), '.mobyshots_mastodon_token'))

        try:
            media_id = masto.media_post(image.name, mime_type=None)
            masto.status_post(message, media_ids=[media_id['id']])
        except (ValueError, IOError) as error:
            self.log('Failed to toot %s: %s' % (self.shot['image'], error.message))
            return False
        self.log('Successfully tooted screenshot for %s' % self.game['title'])
        return True

    def post_screenshot(self):
        self.game = self.get_random_game_with_screenshots()
        screenshots = self.get_screenshots(self.game)

        # pick a screenshot
        self.platform = choice(screenshots.keys())
        self.shot = choice(screenshots[self.platform])

        messages = self.get_messages()

        self.log('Made %s Mobygames API requests.' % self.total_requests)

        with tempfile.NamedTemporaryFile(suffix='.%s' % self.shot['image'].split('.')[-1]) as temp_file:
            img = requests.get(self.shot['image'], stream=True)
            img.raw.decode_content = True
            shutil.copyfileobj(img.raw, temp_file)
            temp_file.flush()

            tweeted = False
            tooted = False
            while not tweeted:
                tweeted = self.tweet(messages['twitter'], temp_file)
            while not tooted:
                tooted = self.toot(messages['mastodon'], temp_file)

        return True


if __name__ == '__main__':

    mobyshots = MobyShots()
    while not mobyshots.post_screenshot():
        pass
